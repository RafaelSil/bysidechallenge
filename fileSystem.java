/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.filesystemoop;

/**
 *
 * @author Rafael
 */
public class fileSystem {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Directory r = new Directory("/root", null);

        Directory dir1 = new Directory("folder1", r);
        Directory dir2 = new Directory("folder1", r);

        r.contents.add(dir1);
        r.contents.add(dir2);

        File f1 = new File("cao", dir1, 100);
        File f2 = new File("gato", dir1, 500);

        dir1.contents.add(f1);
        dir1.contents.add(f2);

        for (int i = 0; i < dir1.contents.size(); i++) {
            System.out.printf("%s",dir1.contents.get(i));
        }

    }

}
