/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.filesystemoop;

/**
 *
 * @author Rafael
 */
public abstract class Entry implements IEntry {

    protected String name;
    protected Directory parentDir;
    protected long created;
    protected long lastUpdated;
    protected long lastAccessed;

    public Entry(String entryName, Directory dir) {
        name = entryName;
        parentDir = dir;
        created = System.currentTimeMillis();
        lastUpdated = System.currentTimeMillis();
        lastAccessed = System.currentTimeMillis();
    }

    @Override
    public boolean delete() {
        if (parentDir == null) {
            return false;
        }
        return parentDir.deleteEntry(this);
    }

    @Override
    public void changeName(String entryName) {
        name = entryName;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getFullPath() {
        if (parentDir == null) {
            return name;
        } else {
            return parentDir.getFullPath() + "/" + name;
        }
    }

    @Override
    public long getCreationTime() {
        return created;
    }

    @Override
    public long getLastUpdatedTime() {
        return lastUpdated;
    }

    @Override
    public long getLastAccessed() {
        return lastAccessed;
    }

    @Override
    public String toString() {
        return "name=" + name + ", parentDir=" + parentDir + ", created=" + created + ", lastUpdated=" + lastUpdated + ", lastAccessed=" + lastAccessed;
    }

    public abstract int size();
}
