/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.filesystemoop;

import java.util.List;

/**
 *
 * @author Rafael
 */
public interface IDirectory {

    int numberOfFiles();

    boolean addEntry(Entry entry);

    boolean deleteEntry(Entry entry);  //no need to return anything, just perform a command

    List<Entry> getContents(); //return the most basic type
}
