/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.filesystemoop;

/**
 *
 * @author Rafael
 */
public class File extends Entry {

    private String content;
    private int size;

    public File(String entryName, Directory dir, int size) {
        super(entryName, dir);
        this.size = size;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String c) {
        content = c;
    }

    public void addContent(String c) {
        content = content + c;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public String toString() {
        //return "File{" + "content=" + content + ", size=" + size + '}';
        return String.format("%nFicheiro: %s Conteudo: %s", super.toString(),content);
    }
}

