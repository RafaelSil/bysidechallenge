/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.filesystemoop;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rafael
 */
public class Directory extends Entry implements IDirectory {

    protected List<Entry> contents;

    public Directory(String entryName, Directory dir) {
        super(entryName, dir);
        contents = new ArrayList<Entry>();
    }

    @Override
    public boolean addEntry(Entry entry) {
        return contents.add(entry);
    }

    @Override
    public boolean deleteEntry(Entry entry) {
        return contents.remove(entry);
    }

    @Override
    public List<Entry> getContents() {
        return contents;
    }

    @Override
    public int numberOfFiles() {
        int count = 0;
        for (Entry e : contents) {
            if (e instanceof Directory) {
                count++; // Directory counts as a file 
                Directory d = (Directory) e;
                count += d.numberOfFiles();
            } else if (e instanceof File) {
                count++;
            }
        }
        return count;
    }

    @Override
    public int size() {
        int size = 0;
        for (Entry e : contents) {
            size += e.size();
        }
        return size;
    }

    @Override
    public String toString() {
        //return "File{" + "content=" + content + ", size=" + size + '}';
        return String.format("%nDirectory: %s, list: %s ", super.toString(), contents);
    }
}
