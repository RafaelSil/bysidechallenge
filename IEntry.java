/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.filesystemoop;

/**
 *
 * @author Rafael
 */
public interface IEntry {

    boolean delete();  //no need to return anything, just perform a command

    void changeName(String name);

    String getName();

    String getFullPath();

    //return class instead of long
    long getCreationTime();

    long getLastUpdatedTime();

    long getLastAccessed();
}
